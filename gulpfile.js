'use strict';

const gulp = require('gulp');
const mjmlEngine = require('mjml').default
const mjml = require('gulp-mjml');
const browserSync = require('browser-sync').create();


// mjmlHtml
function mjmlHtml(done){
    gulp.src('src/*.mjml')
        .pipe(mjml(mjmlEngine, {minify: true}, {beautify: false}))
        .pipe(gulp.dest('./dist'));
    done();
}

// mjmlHtmlBeautify
function mjmlHtmlBeautify(done) {
    gulp.src('src/*.mjml')
        .pipe(mjml(mjmlEngine, {minify: false}, {beautify: true}))
        .pipe(gulp.dest('./dist'));
    done();
}

// BrowserSync
function browsersync(done) {
    browserSync.init({
        server: {
            baseDir: "./dist/"
        },
        port: 3000
    });
    done();
}

// BrowserSync Reload
function browserSyncReload(done) {
    browserSync.reload();
    done();
}

// Watch files
function watchFiles() {
    gulp.watch(['./src/*.mjml'], serve);
}

// define complex tasks
const serve = gulp.series(mjmlHtml, browserSyncReload);
const dev = gulp.series(mjmlHtml, browsersync, watchFiles);

// export tasks

// exports.buildBeautify = buildBeautify;
exports.default = dev;

